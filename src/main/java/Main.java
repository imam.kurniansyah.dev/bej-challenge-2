import generator.Constant;
import generator.GradesFrequenciesGenerator;
import generator.ModusMedianMeanGenerator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        generateMainMenu(scanner);
    }

    private static void generateMainMenu(Scanner scanner) {
        System.out.println(createSeparator());
        System.out.println("Aplikasi Pengolahan Nilai Siswa");
        System.out.println(createSeparator());
        System.out.println("File akan dibaca di " + Constant.fileLocation);
        System.out.println("File akan digenerate di " + Constant.fileLocationGradesFrequency);
        System.out.println();
        System.out.println("Silahkan pilih action :");
        System.out.println("1. Kelompokkan data");
        System.out.println("2. Menyajikan mean, median, dan modus");
        System.out.println("0. Keluar");
        System.out.print("Pilih menu : ");
        int selectedMenu = scanner.nextInt();
        selectMenu(scanner, selectedMenu);
    }

    private static void selectMenu(Scanner scanner, int menu) {
        int selectedMenu;
        switch (menu) {
            case 1:
                GradesFrequenciesGenerator.getInstance().generate();
                System.out.println(createSeparator());
                System.out.println("Menu kelompokkan data");
                System.out.println(createSeparator());
                System.out.println("File akan digenerate di " + Constant.fileLocationGradesFrequency);
                System.out.println();
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Keluar");
                System.out.println();
                System.out.print("Pilih menu : ");
                selectedMenu = scanner.nextInt();
                if (selectedMenu == 1) {
                    generateMainMenu(scanner);
                } else {
                    scanner.close();
                }
                break;
            case 2:
                ModusMedianMeanGenerator.getInstance().generate();
                System.out.println(createSeparator());
                System.out.println("Menu kelompokkan data");
                System.out.println(createSeparator());
                System.out.println("File akan digenerate di " + Constant.fileLocationGradesFrequency);
                System.out.println();
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Keluar");
                System.out.println();
                System.out.print("Pilih menu : ");
                selectedMenu = scanner.nextInt();
                if (selectedMenu == 1) {
                    generateMainMenu(scanner);
                } else {
                    scanner.close();
                }
                break;
        }
    }

    private static String createSeparator() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 40; i++) {
            stringBuilder.append("-");
        }
        return stringBuilder.toString();
    }
}
