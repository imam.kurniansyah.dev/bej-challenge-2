package generator;

public class Constant {
    public static final String fileLocation = "src/main/resources/data_sekolah.csv";
    public static final String fileLocationGradesFrequency = "src/main/resources/data_frekuensi_nilai.txt";
    public static final String fileLocationModusMedianMean = "src/main/resources/data_modus_median_mean.txt";
}
