package generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract public class BaseGenerator {
    List<List<String>> allClass;

    public BaseGenerator() {
        allClass = readFile();
    }

    protected abstract void generate();

    private List<List<String>> readFile() {
        try {
            File file = new File(Constant.fileLocation);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            List<List<String>> allClasses = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                allClasses.add(Arrays.asList(line.split(";")));
            }
            bufferedReader.close();
            return allClasses;
        } catch (IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }
}
