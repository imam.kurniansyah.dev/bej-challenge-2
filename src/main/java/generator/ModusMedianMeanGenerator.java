package generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ModusMedianMeanGenerator extends BaseGenerator {

    private static ModusMedianMeanGenerator instance;

    // make Singleton instance
    public static ModusMedianMeanGenerator getInstance() {
        if (instance == null) {
            instance = new ModusMedianMeanGenerator();
        }
        return instance;
    }

    @Override public void generate() {
        generateGrades();
    }

    private void generateGrades() {
        if (allClass != null) {
            createModusMedianMeanFile(allClass);
        } else {
            System.out.println("File yang dibuka kosong");
        }
    }

    private void createModusMedianMeanFile(List<List<String>> allClass) {
        try {
            File file = new File(Constant.fileLocationModusMedianMean);
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // get Mean for each Class. ex: KelasA to 8 <-- mean
            HashMap<String, Double> meanPerClass = getMeanPerClass(allClass);
            // convert Mean from each class to a List
            ArrayList<Double> listAllMean = new ArrayList<>(meanPerClass.values());
            // sum Mean from all class Mean
            double totalMean = 0.0;
            for (double mean : listAllMean) {
                totalMean += mean;
            }

            // get Mean from all class
            double meanAllClass = totalMean / meanPerClass.size();

            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setMaximumFractionDigits(2);
            bufferedWriter.write("Berikut Hasil Pengolahan Nilai :");
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            bufferedWriter.write("Berikut hasil sebaran data nilai");
            bufferedWriter.newLine();
            bufferedWriter.write("Mean : " + decimalFormat.format(meanAllClass));
            bufferedWriter.newLine();
            bufferedWriter.write("Median : " + decimalFormat.format(getMedian(listAllMean)));
            bufferedWriter.newLine();
            bufferedWriter.write("Modus : " + getModus(listAllMean));
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private HashMap<String, Double> getMeanPerClass(List<List<String>> allClass) {
        // store class name as Key, and Mean for that class as Value
        HashMap<String, Double> meanPerClass = new HashMap<>();
        for (List<String> dataClass : allClass) {
            String className = "";
            float sumGrades = 0;
            for (int j = 0; j < dataClass.size(); j++) {
                if (j == 0) {
                    className = dataClass.get(j);
                } else {
                    sumGrades += Integer.parseInt(dataClass.get(j));
                }
            }

            double mean = sumGrades / (dataClass.size() - 1);
            // store each Class Name and Mean into map
            meanPerClass.put(className, mean);
        }

        // convert Mean from each class to a List
        return meanPerClass;
    }

    private double getMedian(ArrayList<Double> values) {
        // to calculate the Median, values should sort first
        Collections.sort(values);

        if (values.size() % 2 == 1) {
            return values.get((values.size() + 1) / 2 - 1);
        } else {
            double lower = values.get(values.size() / 2 - 1);
            double upper = values.get(values.size() / 2);

            return (lower + upper) / 2.0;
        }
    }

    private long getModus(ArrayList<Double> listAllMean) {
        HashMap<Long, Long> frequencyPerValue = new HashMap<>();
        for (int i = 0; i < listAllMean.size(); i++) {
            long key = Math.round(listAllMean.get(i));
            if (i == 0) {
                frequencyPerValue.put(key, 1L);
            } else {
                if (frequencyPerValue.containsKey(key)) {
                    long frequency = frequencyPerValue.get(key) + 1;
                    frequencyPerValue.put(key, frequency);
                } else {
                    frequencyPerValue.put(key, 1L);
                }
            }
        }

        return Collections.max(frequencyPerValue.values());
    }
}
