package generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class GradesFrequenciesGenerator extends BaseGenerator {

    private static final int MAX_CHAR_CLASS = 7;
    private static final int MAX_CHAR_GRADES_LESS_THAN = 17;
    private static final int MAX_CHAR_FREQUENCIES = 9;

    private static GradesFrequenciesGenerator instance;

    // make Singleton instance
    public static GradesFrequenciesGenerator getInstance() {
        if (instance == null) {
            instance = new GradesFrequenciesGenerator();
        }
        return instance;
    }

    @Override public void generate() {
        generateGrades();
    }

    private void generateGrades() {
        if (allClass != null) {
            createTable(allClass);
        } else {
            System.out.println("File yang dibuka kosong");
        }
    }

    private void createTable(List<List<String>> allClass) {
        try {
            File file = new File(Constant.fileLocationGradesFrequency);
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            generateColumn(bufferedWriter, "KELAS", MAX_CHAR_CLASS);
            generateColumn(bufferedWriter, "NILAI KURANG DARI", MAX_CHAR_GRADES_LESS_THAN);
            generateColumn(bufferedWriter, "FREKUENSI", MAX_CHAR_FREQUENCIES);

            for (int index = 0; index < allClass.size(); index++) {
                List<String> dataClass = allClass.get(index);
                // class name will be assigned by first value of dataClass
                String className = "";
                Set<String> grades = new HashSet<>();
                for (int i = 0; i < dataClass.size(); i++) {
                    if (i == 0) {
                        className = dataClass.get(i);
                    } else {
                        // grades will be filled by dataClass value with index more than 0
                        grades.add(dataClass.get(i));
                    }
                }
                List<String> groupedGrades = new ArrayList<>(grades);

                // prevent print new line when having next data class
                if (index == 0) {
                    bufferedWriter.newLine();
                }
                generateColumn(bufferedWriter, className, MAX_CHAR_CLASS);

                for (int i = 0; i < groupedGrades.size(); i++) {
                    generateColumn(bufferedWriter, groupedGrades.get(i), MAX_CHAR_GRADES_LESS_THAN);
                    // to count frequencies, comparing the value from groupedGrades and value from dataClass
                    int frequency = 0;
                    for (String grade : dataClass) {
                        if (Objects.equals(grade, groupedGrades.get(i))) {
                            frequency++;
                        }
                    }
                    generateColumn(bufferedWriter, String.valueOf(frequency), MAX_CHAR_FREQUENCIES);
                    bufferedWriter.newLine();

                    // prevent print class name on the end of the dataClass
                    if (i < groupedGrades.size() - 1) {
                        generateColumn(bufferedWriter, className, MAX_CHAR_CLASS);
                    }
                }
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void generateColumn(BufferedWriter bufferedWriter, String name, int maxChar) {
        try {
            bufferedWriter.write(name);
            for (int i = 0; i < maxChar - name.length(); i++) {
                bufferedWriter.write(" ");
            }
            bufferedWriter.write(" | ");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
